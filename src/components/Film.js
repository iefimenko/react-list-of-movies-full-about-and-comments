const Film = ({ film, parentFilm, onToggle }) => {
    return (
        <div key={film.id} className={ 'film d-grid' } onClick={ () => onToggle(film.id) }>
            <h3>{film.fname}</h3>
            {parentFilm && <div><span className='textName'>Genre:</span> {film.genre}</div>}
            {parentFilm && <div><span className='textName'>Studio:</span> {film.studio}</div>}
            {parentFilm && <div><span className='textName'>User Rating:</span> {film.userrating}</div>}
            {parentFilm && <div><span className='textName'>Profitability:</span> {film.profitability}</div>}
            {parentFilm && <div><span className='textName'>Rotten Tomatoes Rating:</span> {film.rottentomatoesrating}</div>}
            {parentFilm && <div><span className='textName'>Worldwide Gross:</span> {film.Worldwidegross}</div>}
            {parentFilm && <div><span className='textName'>Year Remix:</span> {film.yearremix}</div>}
        </div>
    )
}

export default Film

import { BrowserRouter as Router, Link } from "react-router-dom"

const Header = () => {
    return (
        <Router>
            <header><Link to='/'>Title</Link></header>
        </Router>
    )
}

export default Header
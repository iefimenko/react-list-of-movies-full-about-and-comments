import { useState } from 'react'

const AddComment = ({ parentFilm, onAdd, onToggle }) => {
    const [text, setText] = useState('')
    const [name, setName] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()

        if(!text) {
            alert('Please add a Comment')
            return
        }

        onAdd({ text, name, parentFilm })

        // setText('')
        // setName('')

        onToggle()
    }

    return (
        <form className='add-form' onSubmit={ onSubmit }>
            <div className='form-control'>
                <input type='text' placeholder='Your comments' value={ text } onChange={ (e) => setText(e.target.value) } />
            </div>
            <div className='form-control'>
                <input type='text' placeholder='Your name'  value={ name } onChange={ (e) => setName(e.target.value) } />
            </div>
            <div className='buttonCover'>
                <input type='submit' value='Save' className='btn btnSubmitComment' />
            </div>
        </form>
    )
}

export default AddComment
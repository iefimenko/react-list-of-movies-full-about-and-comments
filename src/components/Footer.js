import { BrowserRouter as Router, Route } from "react-router-dom"

const Footer = () => {
    return (
        <Router>
            <footer>
                <p>Copyright <a href='https://www.linkedin.com/in/iefimenko'>Ievgen Iefimenko</a></p>
                <Route path='/' exact render={(props) => (
                    <>
                        1,2,3
                    </>
                )} />
            </footer>
        </Router>
    )
}

export default Footer
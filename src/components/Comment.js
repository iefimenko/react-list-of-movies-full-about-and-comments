import { FaTimes } from 'react-icons/fa'

const Comment = ({ comment, onDelete }) => {
    return (
        <div key={comment.id} className={ 'comment d-grid' }>
            <div>{comment.name}: {comment.text}</div>
            <div className='btn-group' role='group' aria-label='Private scchedule'>
                <FaTimes style={{ color: 'red', cursor: 'pointer' }} onClick={() => onDelete(comment.id)} />
            </div>
        </div>
    )
}

export default Comment
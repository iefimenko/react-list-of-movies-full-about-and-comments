import Comment from './Comment'

const Comments = ({ comments, parentFilm, onDelete }) => {
    return (
        <>
            <hr className="w-50"></hr>
            <h1>Comments</h1>

            {comments.filter(comment => {
                console.log(comment.parentFilm, parentFilm)
                if (comment.parentFilm === parentFilm) return true
            }).map(( comment ) => (
                <Comment key={ comment.id } comment={ comment } onDelete={ onDelete } />
            ))}
        </>
    )
}

export default Comments
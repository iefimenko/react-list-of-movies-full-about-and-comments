import { useState } from 'react'
import Film from './Film'

const Films = ({ films, parentFilm, onToggle }) => {
    const [filmToSearch, setFilmToSearch] = useState('')

    return (
        <>
            {!parentFilm && <h1>Movies</h1>}

            {!parentFilm && <input
                placeholder="Search Movie"
                value={filmToSearch}
                onChange={event => {
                    setFilmToSearch(event.target.value)
                    parentFilm = false
                }} />}
            
            {films.filter(film => {
                // console.log(film.id, parentFilm)
                if (film.id === parentFilm) return true
                if (!parentFilm && film.fname.includes(filmToSearch)) return true
            }).map((film) => (
                <Film key={film.id} film={film} parentFilm={parentFilm} onToggle={ onToggle } />
            ))}
        </>
    )
}

export default Films

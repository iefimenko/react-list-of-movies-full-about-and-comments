// this responds for get json data from db.json
export const Fetch = async (path, method, params) => {
    const url = `http://localhost:5030/${path}`
    if(!method) {
        const res = await fetch(url)
        const data = await res.json()
        return data
    } else if(method === 'POST') {
        const res = await fetch(url, {
            method: method,
            headers: {
              'Content-type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        const data = await res.json()
        return data
    } else if(method === 'DELETE') {
        await fetch(`${url}/${params}`, {
            method: method
        })
    }
}

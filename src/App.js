import { useState, useEffect } from 'react'
import Films from './components/Films'
import Comments from './components/Comments'
import AddComment from './components/AddComment'
import { Fetch } from './model/Model'
import Footer from './components/Footer'
import Header from './components/Header'

const App = () => {
  const [showFilmOrComment, setShowFilmOrComment] = useState(true)
  const [parentFilm, setParentFilm] = useState(false)
  const [films, setFilms] = useState([])
  const [comments, setComments] = useState([])

  useEffect(() => {
    const getFilms = async () => {
      const filmsFromServer = await Fetch('films')
      setFilms(filmsFromServer)
    }

    getFilms()
  }, [])

  useEffect(() => {
    const getComments = async () => {
      const commentsFromServer = await Fetch('comments')
      setComments(commentsFromServer)
    }

    getComments()
  }, [])

  // Add session to comment
  const addSession = async (comment) => {
    const data = await Fetch('comments', 'POST', comment)
    setComments([...comments, data])
  }

  // Delete session from comment
  const deleteSession = async (id) => {
    // console.log('delete', id)
    await Fetch('comments', 'DELETE', id)
    setComments(comments.filter((comment) => {
      // console.log(comment.id, '!==', id)
      return comment.id !== id
    }))
  }
  
  const toggleFilmOrComment = (id) => {
    // console.log('film', id)
    setParentFilm(id)
    setShowFilmOrComment(!showFilmOrComment)
  }

  return (
    <div className='App'>
      <Header />
      {films && <Films films={films} parentFilm={parentFilm} onToggle={ toggleFilmOrComment } />}
      {!showFilmOrComment && <Comments comments={comments} parentFilm={parentFilm} onDelete={ deleteSession } />}
      {!showFilmOrComment && <AddComment parentFilm={parentFilm} onAdd={ addSession } onToggle={ toggleFilmOrComment } />}
      <Footer />
    </div>
  );
}

export default App

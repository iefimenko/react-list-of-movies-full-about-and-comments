# Getting Started With Installation

You need to install modules for run Node.

## Available Scripts

### `npm install`


Then you need to install and run JSON Server first.

## Available Scripts

In the root directory, you can run for install:

### `npm install -g json-server`

In the project directory, you can run:

### `json-server --watch db.json --port 5030`